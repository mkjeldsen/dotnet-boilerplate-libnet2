using System.Diagnostics;
using System.Text.RegularExpressions;

if (Args.Count != 1)
{
    throw new ArgumentException("fatal: specify desired new project name; case sensitive");
}

var projNameNew = Args[0];
var boilerplateRegex = new Regex("Boilerplate");

// Namespaces, package references...
ReplaceEmbeddedProjectNames();
// .NET endlessly repeats project names in paths.
ReplaceProjectFiles();
// Also in .sln, and with extra GUID redundancy.
ReplaceSlnFile();
Validate();
File.Delete("Rename.csx");
StageIfGit();

void ReplaceProjectFiles()
{
    foreach (
        var csproj in Directory.EnumerateFiles("src/", "*.csproj", SearchOption.AllDirectories)
    )
    {
        var csprojName = Path.GetFileNameWithoutExtension(csproj);
        var csprojDir = Path.GetDirectoryName(csproj);

        var csprojDirNew = boilerplateRegex.Replace(csprojDir, projNameNew);
        var csprojNameNew = boilerplateRegex.Replace(csprojName, projNameNew);
        var csprojNew = csprojDir + "/" + csprojNameNew + ".csproj";

        Console.WriteLine($"{csproj} -> {csprojNew}");
        Console.WriteLine($"{csprojDir} -> {csprojDirNew}");

        File.Move(csproj, csprojNew);
        Directory.Move(csprojDir, csprojDirNew);
    }
}

void ReplaceEmbeddedProjectNames()
{
    foreach (var f in Directory.EnumerateFiles("src/", "*.cs*", SearchOption.AllDirectories))
    {
        var t = File.ReadAllText(f);
        t = boilerplateRegex.Replace(t, projNameNew);
        // Lazy BOM detection heuristic.
        if (f.EndsWith(".csproj"))
        {
            File.WriteAllText(f, t, Encoding.UTF8);
        }
        else
        {
            File.WriteAllText(f, t);
        }
    }
}

void ReplaceSlnFile()
{
    const string slnPath = "dotnet-boilerplate.sln";

    var slnText = File.ReadAllText(slnPath);
    slnText = ReplaceSlnGuids(slnText);
    slnText = boilerplateRegex.Replace(slnText, projNameNew);

    var slnPathNew = slnPath.Replace("boilerplate", projNameNew.ToLowerInvariant());
    File.WriteAllText(slnPathNew, slnText);
    File.Delete(slnPath);
}

static void Validate()
{
    if (!DotnetTest())
    {
        Console.WriteLine("error: could not \"dotnet test\"");
    }
}

static void StageIfGit()
{
    if (Directory.Exists(".git") && !GitStageChanges())
    {
        Console.WriteLine("warn: could not \"git add Rename.csx '*.sln' src/\"");
    }
}

static string ReplaceSlnGuids(string slnText)
{
    var newGuidByOldGuid = new Dictionary<string, string>();

    string SubstituteGuid(Match m)
    {
        var key = m.Value;
        if (!newGuidByOldGuid.TryGetValue(key, out var val))
            val = newGuidByOldGuid[key] = Guid.NewGuid().ToString("B").ToUpperInvariant();

        return val;
    }

    var msGuidRegex = new Regex("\\{[0-9A-F]{8}-([0-9A-F]{4}-){3}[0-9A-F]{12}\\}");
    var evalSubstituteGuid = new MatchEvaluator(SubstituteGuid);
    return msGuidRegex.Replace(slnText, evalSubstituteGuid);
}

static bool DotnetTest() => BlockProc("dotnet", "test");

static bool GitStageChanges() => BlockProc("git", "add Rename.csx *.sln src/");

static bool BlockProc(string fileName, string arguments) =>
    Process.Start(fileName, arguments).WaitForExit(TimeSpan.FromSeconds(20));
